## 2016武汉黑客马拉松作品提交专用项目

本项目为2016武汉黑客马拉松编程大赛提交作品专用项目，作品提交的步骤为：
- **1 注册开源中国“码云”账号，将自己的项目托管到码云平台。**
- **2 fork本项目，编辑README.md文件将自己的项目链接加入本项目后pull request.pull时注明自己的队伍编号和队伍名称。**
- **3 项目链接名称以“队伍编号-队伍名称-项目名称“的方式进行提交**

---------
### 以下为项目提交区
#### [30-cumtb-成长博物馆android端](https://git.oschina.net/happysunrise/GrowthMuseum.git)  
#### [30-cumtb-成长博物馆服务器端](https://git.oschina.net/happysunrise/growth-museum.git)